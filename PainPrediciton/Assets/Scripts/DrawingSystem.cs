using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


/// <summary>
/// This Class handle the detection of touch, transcription of world and screen position, and 
/// translation of the drawing to values relatives to the chart dimensions
/// </summary>
[RequireComponent(typeof(LineRenderer))]
public class DrawingSystem : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public Camera cam;
    public Canvas UIcanva;
    public UIGridChart chart;
    public Text questionDisplayed;
    public LineRenderer linePrefab;

    private string _category;
    private string _question;


    LineRenderer currentLineRenderer;
    RectTransform image;
    Vector2 lastPos;
    string time;
    List<string> timeStamps = new List<string>();
    public List<LineRenderer> lineList = new List<LineRenderer>();

    float xMin, xMax, yMin, yMax;

    private void Awake()
    {
        


        //currentLineRenderer = GetComponent<LineRenderer>();
        currentLineRenderer = Instantiate(linePrefab, this.transform);
        lineList.Add(currentLineRenderer);
    }

    void OnEnable()
    {
        UIcanva.gameObject.SetActive(false);

        //if (currentLineRenderer.enabled)
        //    currentLineRenderer.enabled = false;

    }

    private void Start()
    {
        currentLineRenderer.enabled = false;
        image = transform.parent.GetComponent<RectTransform>();

        // Get outer bounds of graph
        Vector3[] v = new Vector3[4];
        image.GetComponent<RectTransform>().GetWorldCorners(v);
        Vector3 posLimitConv = v[0];
        Vector2 posLimit = new Vector2(posLimitConv.x, posLimitConv.y);
        xMin = posLimit.x;
        yMin = posLimit.y;

        posLimitConv = v[2];
        posLimit = new Vector2(posLimitConv.x, posLimitConv.y);
        xMax = posLimit.x;
        yMax = posLimit.y;


    }


    bool firstLine = true;
    bool outSide = false;

    public void OnBeginDrag(PointerEventData data)
    {
        outSide = false;

        // Check if line exists
        if (!currentLineRenderer.enabled)
            currentLineRenderer.enabled = true;

        // Get starting point 
        Vector3 posConv = cam.ScreenToWorldPoint(new Vector3(data.pressPosition.x, data.pressPosition.y, 0));
        Vector2 pos = new Vector2(posConv.x, posConv.y);
        
        // If first line drawn, don't take overlaping condition
        if (firstLine)
        {
            currentLineRenderer.positionCount = 1;
            currentLineRenderer.SetPosition(0, pos);
            lastPos = pos;
        }
        else
        {
            // Second and more lines must not overlap with previous one
            bool noOverlap = lastPos.y >= pos.y;
            if (noOverlap)
            {
                // Normal drawing conditions
                currentLineRenderer.positionCount = 1;
                currentLineRenderer.SetPosition(0, pos);
                lastPos = pos;
            }
            else
            {
                // Place first point of new line at least AFTER last line drawn
                currentLineRenderer.positionCount = 1;
                currentLineRenderer.SetPosition(0, lastPos);
                if (lastPos != pos && noOverlap && RectTransformUtility.RectangleContainsScreenPoint(image, data.position, cam))
                {
                    lastPos = pos;
                }
            }
        }
        
        //if (timeStamps.Count != 0)
        //{
        //    timeStamps.Clear();
        //}
        timeStamps.Add(GameManager.Instance.GetTime());
    }

    public void OnDrag(PointerEventData data)
    {
        // Get drawing point coordinate
        Vector3 posConv = cam.ScreenToWorldPoint(new Vector3(data.position.x, data.position.y, 0));
        Vector2 pos = new Vector2(posConv.x, posConv.y);
        
        // Prevent drawing backward
        if (lastPos != pos && lastPos.y >= pos.y)
        {
            // Check if drawing point is inside graph
            if(!RectTransformUtility.RectangleContainsScreenPoint(image, data.position, cam))
            {
                outSide = true;
                
                // If outside graph, stop placement of new points
                if (pos.x < xMin || pos.x > xMax)
                {
                    // if at the end of graph, don't draw anymore
                    if (pos.y > yMax)
                    {

                        // if oustide vertical coordinate, place point at limit of graph
                        if (pos.x < xMin)
                        {
                            pos.x = xMin;
                        }
                        else
                        {
                            pos.x = xMax;
                        }

                        currentLineRenderer.positionCount++;
                        int positionIndex = currentLineRenderer.positionCount - 1;
                        currentLineRenderer.SetPosition(positionIndex, pos);
                        lastPos = pos;
                        //timeStamps.Add(DateTime.Now.Ticks);
                        timeStamps.Add(GameManager.Instance.GetTime());
                    }
                }
            }
            else
            {
                // Normal drawing conditions
                currentLineRenderer.positionCount++;
                int positionIndex = currentLineRenderer.positionCount - 1;
                currentLineRenderer.SetPosition(positionIndex, pos);
                lastPos = pos;
                //timeStamps.Add(DateTime.Now.Ticks);
                timeStamps.Add(GameManager.Instance.GetTime());
                outSide = false;
            }
        }
    }

    public void OnEndDrag(PointerEventData data)
    {
        //get last x position
        Vector3 posConv = cam.ScreenToWorldPoint(new Vector3(data.position.x, data.position.y, 0));
        Vector2 pos = new Vector2(posConv.x, posConv.y);

        // First line placed
        if (firstLine)
        {
            firstLine = false;
        }
        


        // Normal condition ending point
        if (lastPos != pos && lastPos.y >= pos.y && RectTransformUtility.RectangleContainsScreenPoint(image, data.position, cam))
        {
            lastPos = pos;
        }

        // Get last position on graph if endpoint was outside of graph
        if (outSide)
        {
            lastPos = lineList[lineList.Count - 1].GetPosition(lineList[lineList.Count - 1].positionCount - 1);
            outSide = false;
        }
        
        //add next line if line rendered
        if(currentLineRenderer.positionCount > 2)
        {
            currentLineRenderer = Instantiate(linePrefab, this.transform);
            lineList.Add(currentLineRenderer);
        }
        currentLineRenderer.enabled = false;
    }


    // Apply to each lines
    public void SaveAndErase()
    {

        //string results =
        //    DateTime.Now.Ticks + "," +
        //    _category + "," +
        //    "Submit rating" + "," +
        //    _question +
        //    Environment.NewLine;

        GameManager.Instance.SetData(
            GameManager.Instance.GetTime(),
            _category,
            "Submit rating",
            _question,
            "",
            "",
            "",
            ""
            );

        //iterate through lines expect last instanced one (empty)
        int timeStampCount = 0;
        for (int o = 0; o < lineList.Count - 1 ; o++)
        {
            if(lineList[o].positionCount > 2 && lineList.Count > 1)
            {
                int posCount = lineList[o].positionCount;
                for (int i = 0; i < posCount; i++)
                {
                    float yValue = (Camera.main.WorldToScreenPoint(lineList[o].GetPosition(i)).x - chart.origin.x) * chart.gridSize.x / (chart.chartMax.x - chart.origin.x);
                    yValue = Mathf.Clamp(yValue, 0, chart.gridSize.x);
                    float xValue = (Camera.main.WorldToScreenPoint(lineList[o].GetPosition(i)).y - chart.origin.y) * chart.gridSize.y / (chart.chartMax.y - chart.origin.y);
                    xValue = Mathf.Clamp(xValue, 0, chart.gridSize.y);
                    //results +=
                    //    timeStamps[i] + "," +
                    //    "," +
                    //    "," +
                    //    "," +    
                    //    i.ToString() + "," +
                    //    xValue.ToString() + "," +
                    //    yValue.ToString() + "," +
                    //    Environment.NewLine;

                    GameManager.Instance.SetData(
                        timeStamps[timeStampCount],
                        "",
                        "Saved drawing data",
                        "",
                        "",
                        i.ToString(),
                        xValue.ToString("F6"),
                        yValue.ToString("F6")
                        );

                    timeStampCount++;
                }
            }
            else
            {
                //Catch up timeline through bypassed point
                for (int i = 0; i < lineList[o].positionCount; i++)
                {
                    timeStampCount++;
                }

                GameManager.Instance.SetData(
                GameManager.Instance.GetTime(),
                _category,
                "Submit rating with no answer",
                _question,
                "",
                "",
                "",
                ""
                );
            }
        }

        //File.AppendAllText("Results" + ".txt", results);

        //          File.AppendAllText("StandardSaccade_" + UserID + ".txt", value);

        UIcanva.gameObject.SetActive(false);

        //erase the current drawing
        //currentLineRenderer.positionCount = 1;
    }

    public void EraseGraph()
    {
        //Reset timeStamps
        if (timeStamps.Count != 0)
        {
            timeStamps.Clear();
        }

        // erase the current drawing
        for (int i = 0; i < lineList.Count; i++)
        {
            Destroy(lineList[i].gameObject);
            //lineList.Remove(lineList[i]);
        }
        lineList = new List<LineRenderer>();
        firstLine = true;
        // iterate through line renderer and erase

        //Set new line
        currentLineRenderer = Instantiate(linePrefab, this.transform);
        lineList.Add(currentLineRenderer);
        if (currentLineRenderer != null && currentLineRenderer.positionCount > 1)
        {
            currentLineRenderer.positionCount = 1;
        }
        currentLineRenderer.enabled = false;

        // Add erasing log here
        GameManager.Instance.SetData(
                GameManager.Instance.GetTime(),
                _category,
                "Erasing graph",
                _question,
                "",
                "",
                "",
                ""
                );
    }

    public void SetCategory(Text c) { { _category = c.text; } }

    public void SetQuestion(Text q)
    {
        {
            _question = q.text;
            questionDisplayed.text = _question;
        }
    }
}
