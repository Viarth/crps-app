using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttentionTaskWaiter : MonoBehaviour
{
    public Text textDescription;
    public Text textInstructions;
    public GameObject buttonStartTimer;
    public GameObject attentionTaskPanel;
    public GameObject attentionTaskStaryObject;
    public Image TimeProgressionBar;

    public List<DrawingSystem> drawingSystems;
    public List<Toggle> toggles;
    public List<PageSwiper> pageSwipers;

    bool fullRound = true, timerStart = false;


    int index = 0;
    float _timeBeforeSound;
    private void OnEnable()
    {
        _timeBeforeSound = GameManager.Instance.timeBeforeSound;

        TimeProgressionBar.gameObject.transform.parent.gameObject.SetActive(false);


        if (fullRound)
        {
            if (index == 0)
            {
                textDescription.text = "Start Resting";
            }
            else if (index >= GameManager.Instance.attentionTaskTextList.Count)
            {
                index = 0;
                this.gameObject.SetActive(false);
            }
            else
            {
                textDescription.text = GameManager.Instance.attentionTaskTextList[index];
                if (textDescription.text == "Direct your attention to your other hand")
                {
                    textDescription.text = "Direct your attention to your non-painful limb";
                }
                else if (textDescription.text == "Direct your attention to your painful hand")
                {
                    textDescription.text = "Direct your attention to your painful limb";
                }
            }
            index++;
        }
        else if (index >= GameManager.Instance.attentionTaskTextList.Count)
        {
            index = 0;
            this.gameObject.SetActive(false);
            attentionTaskPanel.SetActive(true);
        }
        else
        {
            textDescription.text = GameManager.Instance.attentionTaskTextList[index];
            index = GameManager.Instance.attentionTaskTextList.Count;
        }

        

        textDescription.gameObject.SetActive(true);
        textInstructions.gameObject.SetActive(true);

    //launch timer
        timerActive = true;
        timerStart = false;
        timerPreTaskActive = false;
        buttonStartTimer.SetActive(true);

        //TimeProgressionBar.gameObject.transform.parent.gameObject.SetActive(true);
    }
    private void Update()
    {
        TimerPreTask(timerPreTaskActive);
        if(timerStart)
        TimerGong(timerActive);
    }

    public void StartTimer()
    {
        //30sec before task
        buttonStartTimer.SetActive(false);
        TimeProgressionBar.gameObject.transform.parent.gameObject.SetActive(true);
        timerPreTaskActive = true;

        //120sec before questions


    }

    public void SetFullRound(bool value)
    {
        fullRound = value;
    }

    public void SetIndex(GameObject go)
    {
        index = go.transform.GetSiblingIndex();
    }

    void ResetTasks()
    {
        for (int i = 0; i < drawingSystems.Count; i++)
        {
            drawingSystems[i].EraseGraph();
        }
        for (int i = 0; i < toggles.Count; i++)
        {
            toggles[i].isOn = false;
        }
        //for (int i = 0; i < pageSwipers.Count; i++)
        //{
        //    pageSwipers[i].GoToPanel(1);
        //}
    }

    bool timerPreTaskActive = false;
    float _timeTimerPreTask = 0;
    float _timeToWait = 30;
    void TimerPreTask(bool isActive)
    {
        if (isActive)
        {
            //Bar progress
            TimeProgressionBar.fillAmount = _timeTimerPreTask / _timeToWait;


            if (_timeTimerPreTask < _timeToWait)
            {
                //set data with question category
                _timeTimerPreTask += Time.deltaTime;
            }
            else
            {
                timerPreTaskActive = false;
                _timeTimerPreTask = 0;
                

                //do stuff
                TimeProgressionBar.fillAmount = 0;
                textInstructions.gameObject.SetActive(false);
                textDescription.gameObject.SetActive(true);

                GameManager.Instance.SetData(
            GameManager.Instance.GetTime(),
            "Attention Task",
            "Timer Start",
            index - 1 > 0 ? textDescription.text : "Start resting",
            "",
            "",
            "",
            ""
            );



                //launch task
                timerStart = true;
                GameManager.Instance.PlayGongSound();
                Debug.Log("soundPlayed");
                //set data


            }
        }
        else
        {
            _timeTimerPreTask = 0;
            TimeProgressionBar.fillAmount = 0;
        }
    }

    bool timerActive = false;
    float _timeTimer = 0;
    void TimerGong(bool isActive)
    {
        if (isActive)
        {
            //Bar progress
            TimeProgressionBar.fillAmount = _timeTimer / _timeBeforeSound;


            if (_timeTimer < _timeBeforeSound)
            {
                //set data with question category
                _timeTimer += Time.deltaTime;
            }
            else
            {
                timerActive = false;
                _timeTimer = 0;
                TimeProgressionBar.fillAmount = 0;

                //do stuff
                TimeProgressionBar.gameObject.transform.parent.gameObject.SetActive(false);
                ResetTasks();
                //lauch questions
                attentionTaskStaryObject.SetActive(true);
                this.gameObject.SetActive(false);

                //set data
                GameManager.Instance.SetData(
                    GameManager.Instance.GetTime(),
                    "Attention task",
                    "Timer End",
                    index-1 > 0 ? textDescription.text : "Start resting",               
                    "",
                    "",
                    "",
                    ""
                );

                //play sound once
                GameManager.Instance.PlayGongSound();
                Debug.Log("soundPlayed");
            }
        }
        else
        {
            _timeTimer = 0;
            TimeProgressionBar.fillAmount = 0;
        }
    }
}
