using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

public class GameManager : MonoBehaviour
{
    public bool debugMod = false;

    #region Variables
    [SerializeField]
    private GameObject _MainPanel;

    [SerializeField]
    private InputField _userName;

    [SerializeField]
    private List<GameObject> _panelList;

    [SerializeField]
    private List<GameObject> _questionnairesNeverApplyPanels;

    [Header("Experience Diary")]
    [SerializeField]
    private List<InputField> _experienceDiaryInputFields;

    [SerializeField]
    private GameObject _experienceDiaryPanel;

    [Header("Attention Task")]
    [SerializeField]
    private List<GameObject> _attentionTaskPanels;
    [SerializeField]
    private List<RectTransform> _attentionTaskGroup;
    [SerializeField]
    private GameObject _attentionTaskWaiterPanel;



    [Header("Gong Settings")]
    [SerializeField]
    private AudioClip _sound;
    public float timeBeforeSound = 300;

    AudioSource audioSource;

    private float _timeTimer = 0;
    bool _timerActive = false;


    //DATA
    string _category, _action, _question, _answer;

    int[] neverApplyArray;
    int neverApplySizeArray = 11;
    int neverApplyIndex;

    int[] _experimentDiaryStartCompletion;

    bool painDiaryFirstRead = true;

    FileInfo f;
    StreamWriter w;
    string str_Data;

    public string PanelName { get; set; }
    #endregion

    //Singleton

    private static GameManager _instance;

    public static GameManager Instance { get { return _instance; } }


    #region Unity Method

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;

            
        }

//#if UNITY_EDITOR
//        debugMod = true;
//#else
//        debugMod = false;
//#endifS

    }


    private void Start()
    {
        neverApplySizeArray = _questionnairesNeverApplyPanels.Count;

        //LoadPrefs();
        PlayerPrefs.DeleteAll();
        Caching.ClearCache();


        ShuffleAttentionTask();

        audioSource = this.gameObject.AddComponent<AudioSource>();
        audioSource.playOnAwake = false;
        audioSource.clip = _sound;
        audioSource.volume = 0.5f;
        audioSource.pitch = 0.85f;


        _userName.text = PlayerPrefs.GetString("Username", "");

        f = new FileInfo(Application.persistentDataPath + "\\" + "ResultsNew.txt");
        w = f.CreateText();
        //if (neverApplyArray == null)
        //{
        //    neverApplyArray = new int[neverApplySizeArray];
        //}

        if (true/*!f.Exists*/)
        {
            SetData(
            "Time",
            "Category",
            "Action",
            "Question",
            "Answer",
            "Point ID",
            "x value",
            "y value"
            );
        }


        SetData(
            GameManager.Instance.GetTime(),
            _category != null ? _category : "Sign In",
            "Application Start",
            "",
            "",
            "",
            "",
            ""
            );
    }

    void Update()
    {
        //if (this.forceExit) Application.Quit(); // or reload scene
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //user pressed back key
            Application.Quit();
        }

        if (Input.GetKeyDown(KeyCode.Menu))
        {
            //user pressed menu key
            Application.Quit();
        }

        //TimerGong(_timeTimer, _timerActive);

    }

    private void LateUpdate()
    {
        PanelChecking();
    }

    private void OnApplicationQuit()
    {

        SetData(
            GameManager.Instance.GetTime(),
            _category,
            "Application Quit",
            "",
            "",
            "",
            "",
            ""
            );
        w.Close();


        if (!debugMod)
        {
            //send f on email
            LoadDataInString();
            Send(str_Data);
            SendEmail(str_Data);
        }
    }

    #endregion

    #region UI Buttons Methods
    //These method are called by on click events in Unity Editor
    
    public void SetGongTimer(Text newTime) 
    {
        if (newTime.text != "")
        {
            timeBeforeSound = int.Parse(newTime.text);
        }
        else
        {
            timeBeforeSound = 5 * 60;
        }
    }
    
    public void SetDataUsername(string text)
    {
        PlayerPrefs.SetString("Username", text);
    }

    public void SetDataNeverApplyIndex(int index)
    {
        neverApplyIndex = index;
    }

    public void SetDataActionDoesNotApply(bool value)
    {
        _action = value ? "Tick does not apply" : "Untick does not apply";
    }

    public void SetDataActionNeverApply(bool value)
    {
        _action = value ? "Tick never apply" : "Untick never apply";
    }

    public void SetDataNeverApply(bool value)
    {
        neverApplyArray[neverApplyIndex] = value? 1 : 0;

        SaveSomeArrayToPlayerPref(neverApplyArray, "NeverApplyData");
        PlayerPrefs.Save();
    }

    public void SetDataCustomCategory(string str)
    {
        _category = str;
    }

    public void SetDataCategory(Text cat)
    {
        _category = cat.text;
    }

    public void SetDataAction(string str)
    {
        _action = str;
    }

    public void SetDataSwipe(int oldPageNb, int newPageNb)
    {
        GameManager.Instance.SetData(
            GameManager.Instance.GetTime(),
            _category,
            "Swipe from page " + oldPageNb + " to " + newPageNb,
            "",
            "",
            "",
            "",
            ""
            );
    }

    public void SetDataButton(Text button)
    {
        _action = "Press button: " + button.text;
    }

    public void SetDataQuestion(Text question)
    {
        _question = question.text.Replace(",", "");
    }
    
    public void SetDataAnswer(InputField field)
    {
        _answer = field.text;
    }

    public void SetDataZero()
    {
        _category = "";
        _action = "";
        _question = "";
        _answer = "";
    }

    public void SaveData()
    {
        SetData(
            GameManager.Instance.GetTime(),
            _category,
            _action,
            _question.Replace(" ", "_"),
            _answer.Replace(" ", "_"),
            "",
            "",
            ""
            );
    }

    public void DisableAllPanel()
    {
        for (int i = 0; i < _panelList.Count; i++)
        {
            _panelList[i].SetActive(false);
        }
        _MainPanel.SetActive(false);
    }

    public void PlayVideo(string name)
    {
#if UNITY_EDITOR
        name = Application.dataPath + "/StreamingAssets" + "/" + name;
#elif UNITY_ANDROID
        name = "jar:file://" + Application.dataPath + "!/assets" + name;
#endif

        Handheld.PlayFullScreenMovie(name, Color.black, FullScreenMovieControlMode.CancelOnInput, FullScreenMovieScalingMode.Fill);
    }

    public void ResetAll()
    {
        PlayerPrefs.DeleteAll();
        Caching.ClearCache();
        OnApplicationQuit();
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    public void CloseAndSendData()
    {
        Application.Quit();
    }

    #endregion

    #region PlayersPrefs Methods

    public void SavePrefs()
    {
        PlayerPrefs.SetInt("FirstRead", 1);
        PlayerPrefs.Save();
    }

    public void LoadPrefs()
    {
        //persistent data List
        //read element
        string previousUsername = PlayerPrefs.GetString("Username", "");
        PlayerPrefs.SetString("Username", previousUsername);

        //pain diary one time fillin
        painDiaryFirstRead = PlayerPrefs.GetInt("PainDiaryStart", 1) == 1 ? true : false ;
        PlayerPrefs.SetInt("PainDiaryStart", painDiaryFirstRead ? 1 : 0);

        neverApplyArray = GetSavedIntFromString("NeverApplyData", neverApplySizeArray);
        SaveSomeArrayToPlayerPref(neverApplyArray, "NeverApplyData");

        _experimentDiaryStartCompletion = GetSavedIntFromString("ExpDiaryField", 4);
        SaveSomeArrayToPlayerPref(_experimentDiaryStartCompletion, "ExpDiaryField");

        NeverApplyCheck();
        PainDiaryFirstReadCheck();

        PlayerPrefs.Save();
        // each never applies panels
        // 4 cat, 11 panel

        //https://blog.unity.com/technology/persistent-data-how-to-save-your-game-states-and-settings
        //https://naplandgames.com/blog/2016/11/27/saving-data-in-unity-3d-serialization-for-beginners/
    }


    void SaveSomeArrayToPlayerPref(int[] arrayToSave, string SaveName)
    {
        
        for (int i = 0; i < arrayToSave.Length; i++)
        {
            //idea being here that is saves each float as the basename + the location in array, so float of spot 1 in array would be saved as SaveName1 
            PlayerPrefs.SetInt(SaveName + i, arrayToSave[i]);

            //this is using the name we save, adding on a "Keyword" TotalLength, and saving the array size/number of elements of it, as its own int, to be read later to reconstruct the array.
            PlayerPrefs.SetInt(SaveName + "TotalLength", arrayToSave.Length);
        }
    }

    int[] GetSavedIntFromString(string baseSaveName, int size)
    {
        //create a new array of floats, that has the size of the save name we are looking at.
        int[] ReconstructedArrayFromName = new int[PlayerPrefs.GetInt(baseSaveName + "TotalLength", size)];
        if(ReconstructedArrayFromName.Length != size) 
        {
            ReconstructedArrayFromName = new int[size];
            return ReconstructedArrayFromName;
        }

        for (int i = 0; i < ReconstructedArrayFromName.Length; i++)
        {
            //cycle through each position in the array, and set each spot to that of the saved name we are finding.
            ReconstructedArrayFromName[i] = PlayerPrefs.GetInt(baseSaveName + i, 0);
        }

        return ReconstructedArrayFromName;
    }
    public void PainDiaryInputFieldFilled(int index)
    {
        if(_experienceDiaryInputFields[index].text != "")
        {
            _experimentDiaryStartCompletion[index] = 1;

            SaveSomeArrayToPlayerPref(_experimentDiaryStartCompletion, "ExpDiaryField");
            PlayerPrefs.Save();
        }
    }

    void PainDiaryFirstReadCheck()
    {
        int completionCount = 0;
        for (int i = 0; i < _experimentDiaryStartCompletion.Length; i++)
        {
            completionCount += _experimentDiaryStartCompletion[i];
        }

        if (completionCount == 4)
        {
            Transform mainHandler = _experienceDiaryPanel.transform.parent;
            PageSwiper panelManager = _experienceDiaryPanel.transform.parent.gameObject.GetComponent<PageSwiper>();
            panelManager.totalPages = 1;
            Vector3 newLocation = mainHandler.transform.GetChild(2).transform.position;
            newLocation.x -= 2 * Screen.width;
            mainHandler.transform.GetChild(2).transform.position = newLocation;
            mainHandler.transform.GetChild(0).gameObject.SetActive(false);
            _experienceDiaryPanel.SetActive(false);
        }
    }

    void NeverApplyCheck()
    {
        for (int i = 0; i < _questionnairesNeverApplyPanels.Count; i++)
        {
            if(neverApplyArray[i] == 1)
            {
                Transform questionPanel = _questionnairesNeverApplyPanels[i].transform.parent.parent.parent;
                PageSwiper panelManager = questionPanel.parent.gameObject.GetComponent<PageSwiper>();
                panelManager.totalPages -= 1;

                //get child from panelmanager
                //get child index from toggled one
                //move child after this index

                for (int j = questionPanel.GetSiblingIndex(); j < panelManager.transform.childCount; j++)
                { 
                    Vector3 newLocation = panelManager.transform.GetChild(j).transform.position;
                    newLocation.x -= Screen.width;
                    panelManager.transform.GetChild(j).transform.position = newLocation;
                }

                //_questionnairesNeverApplyPanels[i].GetComponent<Toggle>().isOn = true;
                questionPanel.gameObject.SetActive(false);
            }
        }
    }

    private void PanelChecking()
    {
        bool panelToShow = false;
        for (int i = 0; i < _panelList.Count; i++)
        {
            if (_panelList[i].activeSelf)
            {
                panelToShow = true;
                PanelName = _panelList[i].name;
                return;
            }
        }

        if (!panelToShow)
        {
            _MainPanel.SetActive(true);
            PanelName = _MainPanel.name;

        }
    }
    
    #endregion

    void LoadDataInString()
    {
        StreamReader r = File.OpenText(Application.persistentDataPath + "\\" + "ResultsNew.txt");
        string info = r.ReadToEnd();
        r.Close();
        //assign saved thing to a string
        str_Data = info;
    }

    //This method is defining the timestamp format
    public string GetTime() 
    { 
        //return DateTime.Now.Ticks.ToString("N");
        return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff tt");
    }

    [HideInInspector]
    public List<string> attentionTaskTextList;

    void ShuffleAttentionTask()
    {
        //start at 1 instead of 0 because 0 is resting time before starting
        List<int> indexList = new List<int>();
        indexList.Add(1);
        indexList.Add(2);
        indexList.Add(3);
        indexList.Add(4);

        //define a new order for the list
        Fisher_Yates_CardDeck_Shuffle(indexList);

        //make buttons change order according to list shuffle
        for (int i = 1; i < _attentionTaskGroup.Count; i++)
        {
            _attentionTaskGroup[i].SetSiblingIndex(indexList[i-1]);
        }

        //feed the list for text displayed order
        for (int i = 0; i < _attentionTaskGroup.Count; i++)
        {
            attentionTaskTextList.Add(_attentionTaskGroup[0].parent.GetChild(i).GetComponentInChildren<Text>().text);

        }
        

        //si button play

        //îterate through all switch

        //si seulement cette section

        //switch only to that


    }

    public void PlayGongSound()
    {
        audioSource.Play();
    }

    public static List<int> Fisher_Yates_CardDeck_Shuffle(List<int> aList)
    {

        System.Random _random = new System.Random();

        int myGO;

        int n = aList.Count;
        for (int i = 0; i < n; i++)
        {
            // NextDouble returns a random number between 0 and 1.
            // ... It is equivalent to Math.random() in Java.
            int r = i + (int)(_random.NextDouble() * (n - i));
            myGO = aList[r];
            aList[r] = aList[i];
            aList[i] = myGO;
        }

        return aList;
    }

   

    //This is the method to modify to change dimensions of output, each dimensions is a parameter to add
    //Each modification ask the rewriting of parameters in each call of this SetData() Method
    //Headers writing is made during the Unity Method Start()
    public void SetData(string time, string cat, string action, string question, string answer, string pointID, string xVal, string yVal)
    {
        string separator = ",";
        string results =
            time + separator +
            cat + separator +
            action + separator +
            question + separator +
            answer + separator +
            pointID + separator +
            xVal + separator +
            yVal + separator + "\n";

        w.Write(results);

    }

    //Method for sending data by email
    public static void Send(string message)
    {
        ////This is a dedicated Gmail adress
        //string SenderMailAddress = "pain.prediction.data@gmail.com";
        //string SenderPassword = "PainPredicition-jklZG438%";
        //string ReceiverMailAddress = "arthur.trivier@epfl.ch";


        //MailMessage mail = new MailMessage();
        //mail.From = new MailAddress(SenderMailAddress);
        //mail.To.Add(ReceiverMailAddress);
        //mail.Subject = "User " + PlayerPrefs.GetString("Username", "") + " sent data";
        //mail.Body = message;

        //SmtpClient smtp = new SmtpClient();
        //smtp.Host = "smtp.gmail.com";
        //smtp.Port = 587;
        //smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
        //smtp.Credentials = new System.Net.NetworkCredential(SenderMailAddress, SenderPassword) as ICredentialsByHost;
        //smtp.EnableSsl = true;

        //ServicePointManager.ServerCertificateValidationCallback =
        //        delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) {
        //            return true;
        //        };
        //smtp.Send(mail);
        //Debug.Log("Email sent");
    }

    public void SendEmail(string data)
    {
        string email = "pain.prediction.data@gmail.com";
        string subject = MyEscapeURL("User " + PlayerPrefs.GetString("Username", "") + " sent data");
        string body = MyEscapeURL(data);
        Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
        Debug.Log("Email sent");

    }

    string MyEscapeURL(string URL)
    {
        return WWW.EscapeURL(URL).Replace("+", "%20");
    }





}
